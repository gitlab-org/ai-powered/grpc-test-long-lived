import grpc
import asyncio
import logging
from concurrent.futures import ThreadPoolExecutor
import workflow_test_ping_pb2
import workflow_test_ping_pb2_grpc
import time
from typing import AsyncIterable
import os
from grpc_reflection.v1alpha import reflection

_cleanup_coroutines = []

class WorkflowTestPing(workflow_test_ping_pb2_grpc.WorkflowTestPingServicer):
    async def PingPong(
        self,
        request_iterator: AsyncIterable[workflow_test_ping_pb2.PingPing],
        context: grpc.aio.ServicerContext,
    ) -> AsyncIterable[workflow_test_ping_pb2.PongPong]:
        logging.info("Starting PingPong")
        ping_count=0
        async for ping in request_iterator:
            logging.info("Received ping at time %s with pongCount %d and peer %s", ping.time, ping.pongCount, context.peer())
            ping_count += 1
            yield workflow_test_ping_pb2.PongPong(time=int(time.time()), pingCount=ping_count)


async def serve(address: str) -> None:
    server = grpc.aio.server(ThreadPoolExecutor())
    workflow_test_ping_pb2_grpc.add_WorkflowTestPingServicer_to_server(WorkflowTestPing(), server)
    SERVICE_NAMES = (
        workflow_test_ping_pb2.DESCRIPTOR.services_by_name['WorkflowTestPing'].full_name,
        reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(SERVICE_NAMES, server)
    server.add_insecure_port(address)
    await server.start()

    async def server_graceful_shutdown():
        logging.info("Starting graceful shutdown...")
        await server.stop(0)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    try:
        port = os.environ.get("PORT", "5555")
        loop.run_until_complete(serve(f"[::]:{port}"))
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
