## Running

### Setup

```
poetry shell
poetry install
```

### Run

```
python server.py
```

```
python client.py
```

### Connect to a remote server

```
GRPC_SERVER_URL=<HOSTNAME>:443 python client.py
```

### Debug

```
export GRPC_VERBOSITY=DEBUG
export GRPC_TRACE=http
```

### Recompile protobufgs

```
python -m grpc_tools.protoc -I./ --python_out=. --pyi_out=. --grpc_python_out=. workflow_test_ping.proto
```
