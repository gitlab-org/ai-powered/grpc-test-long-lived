from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class PingPing(_message.Message):
    __slots__ = ("time", "pongCount")
    TIME_FIELD_NUMBER: _ClassVar[int]
    PONGCOUNT_FIELD_NUMBER: _ClassVar[int]
    time: int
    pongCount: int
    def __init__(self, time: _Optional[int] = ..., pongCount: _Optional[int] = ...) -> None: ...

class PongPong(_message.Message):
    __slots__ = ("time", "pingCount")
    TIME_FIELD_NUMBER: _ClassVar[int]
    PINGCOUNT_FIELD_NUMBER: _ClassVar[int]
    time: int
    pingCount: int
    def __init__(self, time: _Optional[int] = ..., pingCount: _Optional[int] = ...) -> None: ...
